# LearnGolang
本项目记录自己学习Golang过程中遇到的一些优秀项目。包括一些Go语言的学习资源和一些优秀的Go项目代码。尽可能的使用Go，尽可能的使用Go进行Hack。作者：[0e0w](https://github.com/0e0w/LearnGolang)

本项目创建于2020年8月10日，最近一次更新时间为10月07日。

- [0x01-Golang书籍](https://github.com/0e0w/LearnGolang#0x01-golang%E4%B9%A6%E7%B1%8D)
- [0x02-Golang视频](https://github.com/0e0w/LearnGolang#0x02-golang%E8%A7%86%E9%A2%91)
- [0x03-Golang论坛](https://github.com/0e0w/LearnGolang#0x03-golang%E8%AE%BA%E5%9D%9B)
- [0x04-Golang框架](https://github.com/0e0w/LearnGolang#0x04-golang%E6%A1%86%E6%9E%B6)
- [0x05-Golang项目](https://github.com/0e0w/LearnGolang#0x05-golang%E9%A1%B9%E7%9B%AE)
- [0x06-Golang安全](https://github.com/0e0w/LearnGolang#0x06-golang%E5%AE%89%E5%85%A8)
- [0x07-Golang逆向](https://github.com/0e0w/LearnGolang#0x07-golang%E9%80%86%E5%90%91)

## 0x01-Golang书籍

一、基础入门：

- [x] [《Go语言圣经》](http://books.studygolang.com/gopl-zh/)@柴树杉
- [x] [《Go入门指南》](https://github.com/Unknwon/the-way-to-go_ZH_CN)@无闻译
- [x] [《Go语言四十二章经》](https://github.com/ffhelicopter/Go42)@李骁
- [x] [《Go编程时光》](https://github.com/iswbm/GolangCodingTime)@王炳明
- [x] [《胡文Go.ogle》](https://github.com/0e0w/LearnGolang/blob/master/0x01-Golang%E4%B9%A6%E7%B1%8D/01-%E5%9F%BA%E7%A1%80%E5%85%A5%E9%97%A8/%E8%83%A1%E6%96%87Go.ogle.pdf)@Fango
- [x] [《Go语言标准库》](https://github.com/polaris1119/The-Golang-Standard-Library-by-Example)@徐新华
- [x] [《Go简易教程》](https://github.com/songleo/the-little-go-book_ZH_CN)@李松松
- [x] [《Go语言101》](https://github.com/golang101/golang101)@老貘
- [ ] [《Go by Example 中文》](https://gobyexample-cn.github.io) @everyx
- [ ] [《Go语言简明教程》](https://geektutu.com/post/quick-golang.html)@geektutu
- [ ] [《实效Go编程》](https://github.com/bingohuang/effective-go-zh-en)@黄庆兵
- [ ] [《Golang100天从新手到大师》](https://github.com/rubyhan1314/Golang-100-Days)@韩茹
- [ ] [《神奇的Go语言》](https://wiki.jikexueyuan.com/project/magical-go/)
- [ ] [《快学Go语言》](https://zhuanlan.zhihu.com/quickgo)
- [ ] [《Go命令教程》](https://github.com/hyper0x/go_command_tutorial)
- [ ] [《Go RPC 开发指南》](http://books.studygolang.com/go-rpc-programming-guide)
- [ ] [《通过测试学习Go语言》](http://books.studygolang.com/learn-go-with-tests)
- [ ] [《Mastering_Go_ZH_CN》](http://books.studygolang.com/Mastering_Go_ZH_CN/)
- [ ] [《Go网络编程》](http://books.studygolang.com/NPWG_zh)
- [ ] [《w3cschool-Go教程》](https://www.w3cschool.cn/go/)
- [ ] [《Go指南》](https://tour.go-zh.org/welcome/1)
- [ ] [《学习Go语言》](https://github.com/mikespook/Learning-Go-zh-cn)@mikespook
- [ ] [《Go开发者路线图》](https://github.com/Quorafind/golang-developer-roadmap-cn)
- [ ] [《go-perfbook》](https://github.com/dgryski/go-perfbook)
- [ ] [《golang-notes》](https://github.com/cch123/golang-notes)
- [ ] [《Go实战开发》](https://github.com/astaxie/go-best-practice)
- [ ] [《跟煎鱼学Go》](https://eddycjy.com/go-categories)
- [ ] [《365天深入理解Golang》](https://github.com/0e0w/365Golang)@0e0w

二、高手进阶：

- [x] [《Go官方博客》](https://blog.golang.org)
- [x] [《Go官方包文档》](https://golang.org/pkg)
- [x] [《Go专家编程》](http://books.studygolang.com/GoExpertProgramming) @任洪彩
- [x] [《Go语言高级编程》](https://github.com/chai2010/advanced-go-programming-book)@柴树杉
- [ ] [《Go语法树入门》](https://github.com/chai2010/go-ast-book)@柴树杉
- [ ] [《Go2编程指南》](https://chai2010.cn/go2-book)@柴树杉
- [ ] [《Go语言原本》](https://github.com/golang-design/under-the-hood)@欧长坤
- [ ] [《深入解析Go》](https://github.com/tiancaiamao/go-internals)@tiancaiamao
- [ ] [《剑指Offer - Golang实现》](https://github.com/DinghaoLI/Coding-Interviews-Golang)@DinghaoLI
- [ ] [《Uber Go语言编码规范》](https://github.com/xxjwxc/uber_go_guide_cn)@xxjwxc
- [ ] [《Go语言最佳实践》](https://github.com/llitfkitfk/go-best-practice)@田浩
- [ ] [《Go语言设计模式》](https://github.com/senghoo/golang-design-pattern)@senghoo
- [ ] [《7天用Go从零实现系列》](https://github.com/geektutu/7days-golang)@geektutu
- [ ] [《Go语言并发编程》](https://github.com/cizixs/go-concurrency-programming)@cizixs
- [ ] [《Go 1.5 源码剖析》](https://github.com/qyuhen/book)@雨痕
- [ ] [《深入Go并发编程研讨课》](https://github.com/smallnest/dive-to-gosync-workshop)@smallnest
- [ ] [《go-internals-CN》](https://github.com/go-internals-cn/go-internals)@ray-g
- [ ] [《Go-Questions》](https://github.com/qcrao/Go-Questions/wiki)@qcrao
- [ ] [《Go编程语言安全编码实践指南》](https://github.com/OWASP/Go-SCP)@OWASP

三、算法学习：

- [x] [《LeetCode-Go》](https://github.com/halfrost/LeetCode-Go)@halfrost
- [ ] [《算法学习 Golang 版》](https://github.com/skyhee/Algorithms-Learning-With-Go)@skyhee
- [ ] [《数据结构和算法-Golang实现》](https://github.com/hunterhug/goa.c)@hunterhug
- [ ] [《啊哈!算法》](https://github.com/eruca/aha)@eruca
- [ ] [《algorithm》](https://github.com/dreddsa5dies/algorithm) @algorithm
- [ ] [《awesome-golang-leetcode》](https://github.com/kylesliu/awesome-golang-leetcode)@kylesliu
- [ ] [《The Algorithms - Go》](https://github.com/TheAlgorithms/Go)@TheAlgorithms
- [ ] [《Go的数据结构和算法思维》](https://github.com/careermonk/data-structures-and-algorithmic-thinking-with-go)@careermonk
- [ ] [《fucking-algorithm》](https://github.com/labuladong/fucking-algorithm)@labuladong
- [ ] [《通过go实现基本的23种设计模式》](https://github.com/medasz/Design-Patterns)

四、Web编程：

- [x] [《Go Web编程》](https://github.com/astaxie/build-web-application-with-golang/blob/master/zh/preface.md)@谢孟军
- [ ] [《通过例子学习Go Web编程》](http://books.studygolang.com/gowebexamples)
- [ ] [《Go语言Web应用开发》](https://github.com/unknwon/building-web-applications-in-go)@unknwon
- [ ] [《web-dev-golang-anti-textbook》](https://github.com/thewhitetulip/web-dev-golang-anti-textbook)@thewhitetulip

五、英文原著

- [ ] [《go-internals》](https://github.com/teh-cmc/go-internals)
- [x] [《Go Bootcamp》](http://golangbootcamp.com/)
- [ ] [《automateGo》](https://github.com/dreddsa5dies/automateGo)
- [ ] [《gostart》](https://github.com/alco/gostart)
- [ ] [《Black Hat Go》](https://github.com/blackhat-go/bhg)
- [ ] [《automateGo》](https://github.com/dreddsa5dies/automateGo)
- [ ] [《Go in Action》](https://www.manning.com/books/go-in-action)
- [ ] [《Introducing Go》]()
- [ ] [《The-Little-Go-Book》](https://github.com/karlseguin/the-little-go-book)
- [ ] [《Go Recipes》]()
- [ ] [《Get Programming with Go》]()
- [ ] [《The Ultimate Go Study Guide》](https://github.com/hoanhan101/ultimate-go)@hoanhan101
- [x] [《An Introduction to Programming in Go》](http://www.golang-book.com/)
- [ ] [《Go by Example》](https://github.com/mmcgrana/gobyexample)
- [ ] [《A Go Developer's Notebook》](https://leanpub.com/GoNotebook/read)
- [ ] [《An Introduction to Programming in Go》](http://www.golang-book.com/books/intro)
- [ ] [《Build Web Application with Golang》](https://www.gitbook.com/book/astaxie/build-web-application-with-golang/details)
- [ ] [《Building Web Apps With Go》](https://www.gitbook.com/book/codegangsta/building-web-apps-with-go/details)
- [ ] [《A Go Developer's Notebook》](https://leanpub.com/GoNotebook/read)
- [ ] [《Build Web Application with Golang》](https://www.gitbook.com/book/astaxie/build-web-application-with-golang/details)
- [ ] [《Building Web Apps With Go》](https://www.gitbook.com/book/codegangsta/building-web-apps-with-go/details)
- [ ] [《Go 101》](https://go101.org/) 
- [ ] [《Go Succinctly》](https://github.com/thedevsir/gosuccinctly)
- [ ] [《GoBooks》](https://github.com/dariubs/GoBooks)
- [ ] [《Learning Go》](https://www.miek.nl/downloads/Go/Learning-Go-latest.pdf)
- [ ] [《Network Programming With Go》](https://jan.newmarch.name/go/)
- [ ] [《Spaceship Go A Journey to the Standard Library》](https://blasrodri.github.io/spaceship-go-gh-pages/)
- [ ] [《The Go Programming Language》](http://www.gopl.io/)
- [ ] [《Web Application with Go the Anti-Textbook》](https://github.com/thewhitetulip/web-dev-golang-anti-textbook/)
- [ ] [《Writing A Compiler In Go》](https://compilerbook.com/)
- [ ] [《Writing An Interpreter In Go》](https://interpreterbook.com/)
- [ ] [《Get Programming with Go》]()
- [ ] [《101+ coding interview problems in Go》](https://github.com/hoanhan101/algo)

六、实体书籍：

- [x] [实体书《Go程序设计语言》](https://item.jd.com/12187988.html)@李道兵译
- [x] [实体书《Go语言实战》](https://item.jd.com/12136974.html)@李兆海译
- [x] [实体书《Go语言高级编程》](https://item.jd.com/12647494.html)@柴树杉 曹春晖
- [x] [实体书《Go语言编程之旅》](https://item.jd.com/12685249.html)@陈剑煜 徐新华
- [x] [实体书《Go语言学习笔记》](https://item.jd.com/56109131513.html)@雨痕
- [x] [实体书《Go语言专家编程》](https://item.jd.com/12920392.html)@任洪彩
- [ ] [实体书《Go语言趣学指南》](https://item.jd.com/12826232.html) @黄健宏译
- [ ] [实体书《Go语言并发之道》](https://item.jd.com/12504386.html)@凯瑟琳
- [ ] [实体书《GoWeb编程》](https://item.jd.com/12252845.html)@黄健宏译
- [ ] [实体书《Go语言核心编程》](https://item.jd.com/12437839.html)@李文塔
- [ ] [实体书《Go并发编程实战》](https://item.jd.com/12063141.html)@郝林
- [ ] [实体书《Go语言开发实战》](https://item.jd.com/12622679.html)@千锋教育

七、杂七杂八

- [ ] [GoBooks](https://asmcn.icopy.site/awesome/GoBooks)
- [ ] [Go 语言学习资料与社区索引](https://github.com/unknwon/go-study-index)
- [ ] [Go 资源大全中文版](https://github.com/jobbole/awesome-go-cn)
- [ ] [gyuho/learn#go](https://github.com/gyuho/learn#go)
- [ ] [hackerrankGo](https://github.com/dreddsa5dies/hackerrankGo)
- [ ] [Golang 汇编入门知识总结](https://mp.weixin.qq.com/s/tN27osC6K0NM-Laj9MbtsA)
- [ ] [Go语言招聘](https://gocn.vip/jobs)
- [ ] [Gopher 阅读清单](https://github.com/enocom/gopher-reading-list)
- [ ] [Go 知识图谱](https://github.com/gocn/knowledge)
- [ ] https://github.com/golang-design/history
- [ ] [golang-study-base-master](https://github.com/carolxiong/golang-study-base-master)

## 0x02-Golang视频

- [ ] [《Go编程基础》](https://github.com/unknwon/go-fundamental-programming)@无闻
- [ ] [《Go语言第一课》](https://www.imooc.com/learn/345)@郝林
- [ ] [《Go语言核心36讲》](https://time.geekbang.org/column/intro/112)@郝林
- [ ] [《20个小时快速入门go语言-上》]( https://www.bilibili.com/video/BV1UW411x7v2)@黑马程序员
- [ ] [《20个小时快速入门go语言-中》](https://www.bilibili.com/video/BV1UW411x7Ve)@黑马程序员
- [ ] [《20个小时快速入门go语言-下》](https://www.bilibili.com/video/BV17W411W7hm)@黑马程序员
- [ ] [《Go Syntax LiveBytes》](https://www.youtube.com/channel/UCCgGRKeRM1b0LTDqqb4NqjA)

## 0x03-Golang论坛

- [ ] [Go夜读](https://github.com/talkgo/night)
- [ ] [Go学习之路](https://github.com/yangwenmai/learning-golang)
- [ ] [Reddit 的go社区](https://www.reddit.com/r/golang)
- [ ] [golang-nuts](https://groups.google.com/group/golang-nuts/)
- [ ] [GopherChina](https://gopherchina.org/)
- [ ] https://gocn.vip
- [ ] https://igo.pub
- [ ] https://talkgo.org
- [ ] https://studygolang.com
- [ ] https://www.golangtc.com
- [ ] https://www.golangschool.com
- [ ] https://learnku.com/go

## 0x04-Golang框架

- [ ] https://github.com/avelino/awesome-go
- [ ] https://github.com/jobbole/awesome-go-cn
- [ ] https://github.com/hyper0x/awesome-go-China

一、Web框架

- [x] https://github.com/astaxie/beego
- [ ] https://github.com/gin-gonic/gin
- [ ] https://github.com/kataras/iris
- [ ] https://github.com/revel/revel
- [ ] https://github.com/go-martini/martini
- [ ] https://go-macaron.com/
- [ ] https://echo.labstack.com/
- [ ] https://github.com/revel/revel
- [ ] https://github.com/go-martini/martini
- [ ] https://github.com/gin-gonic/gin
- [ ] https://github.com/kataras/iris
- [ ] https://github.com/savsgio/atreugo
- [ ] https://github.com/tal-tech/go-zero
- [ ] https://github.com/gohugoio/hugo

二、爬虫框架

- [x] https://github.com/gocolly/colly
- [ ] https://github.com/PuerkitoBio/goquery
- [ ] https://github.com/jaeles-project/gospider
- [ ] https://github.com/china-muwenbo/gospider

三、HTTP框架

- [ ] https://github.com/julienschmidt/httprouter
- [ ] https://github.com/valyala/fasthttp
- [ ] https://github.com/gorilla/mux
- [ ] https://github.com/alexedwards/scs
- [ ] https://github.com/zhshch2002/goreq
- [ ] https://github.com/projectdiscovery/rawhttp

四、JSON解析

- [ ] https://github.com/json-iterator/go
- [ ] https://github.com/buger/jsonparser

五、数据库以及ORM

- [ ] https://github.com/syndtr/goleveldb
- [ ] https://github.com/boltdb/bolt
- [ ] https://github.com/go-sql-driver/mysql
- [ ] https://github.com/pingcap/tidb
- [ ] https://github.com/ideawu/ssdb
- [ ] https://github.com/jinzhu/gorm

六、中间件

- [ ] https://github.com/go-redis/redis
- [ ] https://github.com/olivere/elastic
- [ ] https://github.com/justinas/alice

七、日志框架

- [ ]  https://github.com/uber-go/zap
- [ ] https://github.com/go-clog/clog
- [ ] https://github.com/rs/zerolog
- [ ] https://github.com/sirupsen/logrus

八、错误处理

- [ ] https://github.com/pkg/errors
- [ ] https://github.com/rotisserie/eris

九、消息队列

- [ ]  https://github.com/nsqio/nsq
- [ ]  https://github.com/MasslessParticle/GoQ

十、Service Mesh

- [ ] https://github.com/istio/istio

十一、RPC

- [ ] https://github.com/smallnest/rpcx

- [ ] https://github.com/grpc/grpc-go

十二、协程池

- [ ]  https://github.com/panjf2000/ants
- [ ]  https://github.com/alitto/pond

十三、视觉图像处理

- [ ] https://github.com/anthonynsimon/bild
- [ ] https://github.com/3d0c/gmf
- [ ] https://github.com/hybridgroup/gocv
- [ ] https://github.com/MindorksOpenSource/gogeom

十四、网络框架

- [ ] https://github.com/xtaci/kcp-go


十五、测试框架

- [ ] https://github.com/h2non/gock

- [ ] https://github.com/360EntSecGroup-Skylar/goreporter

十六、邮箱框架

- [ ] https://github.com/jordan-wright/email
- [ ] https://github.com/foxcpp/maddy

十七、图形化

- [ ] https://github.com/andlabs/ui
- [ ] https://github.com/kpfaulkner/goui

十八、RSS框架

- [ ] https://github.com/mmcdole/gofeed
- [ ] https://github.com/gorilla/feeds
- [ ] https://github.com/nkanaev/gofeed

## 0x05-Golang项目

〇、项目搜索

- [ ] https://github.com/trending/go

一、CMS网站

- [x] https://github.com/gogs/gogs
- [x] https://github.com/ponzu-cms/ponzu
- [ ] https://github.com/qor/qor

二、钓鱼工具

- [x] https://github.com/gophish/gophish
- [x] https://github.com/hacklcx/HFish

三、娱乐游戏

- [ ] https://github.com/loig/ld47
- [ ] https://github.com/Akatsuki-py/OpenPokemonRed

四、优秀项目

- [ ] https://github.com/iawia002/annie
- [ ] https://github.com/schollz/croc
- [ ] https://github.com/lsds/KungFu
- [ ] https://github.com/mit-pdos/biscuit
- [ ] https://github.com/bradfitz/shotizam
- [ ] https://github.com/sqreen/go-agent
- [ ] https://github.com/didi/sharingan
- [ ] https://github.com/huhu/go-search-extension
- [ ] https://github.com/berty/berty
- [ ] https://github.com/u-root/gobusybox
- [ ] https://github.com/nkanaev/yarr
- [ ] https://github.com/AgeloVito/proxypool

## 0x06-Golang安全

〇、HackWithGo

- [ ] [Hacking-with-Go ](https://github.com/parsiya/Hacking-with-Go)@parsiya
- [ ] [My Go Security Projects](https://github.com/parsiya/Go-Security) @parsiya
- [ ] [Hacking-With-Golang](https://github.com/AV1080p/Hacking-With-Golang) @AV1080p
- [ ] [Go-Learning-With-Hack](https://github.com/lazybootsafe/Go-Learning-With-Hack) @lazybootsafe
- [x] [goHackTools](https://github.com/dreddsa5dies/goHackTools) @dreddsa5dies
- [x] [hackerrankGo ](https://github.com/dreddsa5dies/hackerrankGo)@dreddsa5dies
- [x] [1000GoExamples](https://github.com/dreddsa5dies/1000GoExamples) @dreddsa5dies
- [ ] https://github.com/tomnomnom/hacks
- [ ] https://github.com/zheng-ji/ToyCollection
- [ ] https://github.com/tprynn/web-methodology
- [ ] https://github.com/topics/security?l=go
- [ ] https://github.com/topics/hacking?l=go
- [ ] https://github.com/topics/scanner?l=go
- [ ] https://github.com/topics/hacking-tool?l=go
- [ ] https://github.com/topics/security-tools?l=go
- [ ] https://github.com/topics/security-audit?l=go
- [ ] https://github.com/topics/dirbuster?l=go
- [ ] https://github.com/topics/information-gathering?l=go
- [ ] https://github.com/topics/cve?l=go
- [ ] https://github.com/topics/vulnerability-scanner?l=go
- [ ] https://github.com/topics/vulnerability?l=go
- [ ] https://github.com/topics/xss?l=go
- [ ] https://github.com/topics/poc?l=go
- [ ] https://github.com/topics/redteam?l=go
- [ ] https://github.com/topics/tunnel?l=go
- [ ] https://github.com/search?l=Go&q=cve
- [ ] https://github.com/search?l=Go&q=portscan
- [ ] https://github.com/search?l=Go&q=%E6%89%AB%E6%8F%8F

一、域名扫描

​	https://github.com/topics/subdomain?l=go
​	https://github.com/topics/subdomains?l=go
​	https://github.com/search?l=Go&q=subdomain

- [x] https://github.com/knownsec/ksubdomain
- [x] https://github.com/SanSYS/subdscan
- [x] https://github.com/evilsocket/dnssearch
- [ ] https://github.com/haccer/subjack
- [ ] https://github.com/hahwul/ras-fuzzer
- [ ] https://github.com/OWASP/Amass
- [ ] https://github.com/rverton/redAsset
- [ ] https://github.com/subfinder/goaltdns
- [ ] https://github.com/QSoloX/whoisyou
- [ ] https://github.com/gwen001/github-subdomains
- [ ] https://github.com/tomsteele/blacksheepwall
- [ ] https://github.com/tomnomnom/assetfinder
- [ ] https://github.com/projectdiscovery/shuffledns
- [ ] https://github.com/projectdiscovery/chaos-client
- [ ] https://github.com/projectdiscovery/dnsprobe
- [ ] https://github.com/projectdiscovery/subfinder
- [ ] https://github.com/scanterog/crawler
- [ ] https://github.com/jimen0/brute
- [ ] https://github.com/iepathos/brutall
- [ ] https://github.com/anshumanbh/brutesubs
- [ ] https://github.com/Kevintheminion18/assetfinder
- [ ] https://github.com/McRaeAlex/domainutils
- [ ] https://github.com/kevinborras/GokGok
- [ ] https://github.com/attacker34/brute
- [ ] https://github.com/evilsocket/dnssearch
- [ ] https://github.com/Ice3man543/SubOver
- [ ] https://github.com/haccer/subjack
- [ ] https://github.com/projectdiscovery/subfinder
- [ ] https://github.com/LukaSikic/subzy
- [ ] https://github.com/tomnomnom/assetfinder
- [ ] https://github.com/gwen001/github-subdomains
- [ ] https://github.com/hahwul/ras-fuzzer
- [ ] https://github.com/projectdiscovery/shuffledns
- [ ] https://github.com/incogbyte/shosubgo |shodan接口搜索子域
- [ ] https://github.com/tismayil/rsdl | 使用ping方法进行子域名扫描
- [ ] https://github.com/projectdiscovery/dnsprobe
- [ ] https://github.com/theblackturtle/fprobe
- [ ] https://github.com/zsdevX/DarkEye
- [ ] https://github.com/bobesa/go-domain-util
- [ ] https://github.com/codeexpress/subdomainrecon
- [ ] https://github.com/netevert/delator
- [ ] https://github.com/edoardottt/scilla
- [ ] https://github.com/SanSYS/subdscan
- [ ] https://github.com/MilindPurswani/takemeon
- [ ] https://github.com/yghonem14/cngo
- [ ] https://github.com/fengdingbo/subdomain-scanner | 国人写的
- [ ] https://github.com/pwn1sher/CertShooter
- [ ] https://github.com/Asjidkalam/SubRecon | 子域名接管扫描
- [ ] https://github.com/51gn3d/stko
- [ ] https://github.com/Sam-Lane/subway
- [ ] https://github.com/mosunit/SubdomainEnumerator
- [ ] https://github.com/mcrouse911/subdomainfinder
- [ ] https://github.com/AnikHasibul/crtscan | 从证书透明性日志扫描子域
- [ ] https://github.com/ManShum812/Automating-Scan-Live-Subdomain | 扫描实时子域
- [ ] https://github.com/foae/extract-subdomains-from-https
- [ ] https://github.com/Any3ite/subDomainFucker
- [ ] https://github.com/medasz/subDomainsBrute
- [ ] https://github.com/denismitr/subguess
- [ ] https://github.com/jonhadfield/subtocheck
- [ ] https://github.com/minix357/subsearch
- [ ] https://github.com/dryvenn/crawler
- [ ] https://github.com/z3dc0ps/altsub-pro
- [ ] https://github.com/Redislabs-Solution-Architects/dnstracer
- [ ] https://github.com/goodlandsecurity/subfinder
- [ ] https://github.com/scanterog/crawler
- [ ] https://github.com/iepathos/brutall
- [ ] https://github.com/Kevintheminion18/assetfinder
- [ ] https://github.com/McRaeAlex/domainutils
- [ ] https://github.com/subfinder/goaltdns
- [ ] https://github.com/netevert/delator
- [ ] https://github.com/nscuro/fdnssearch
- [ ] https://github.com/amar-myana/certdomainfinder
- [ ] https://github.com/jimen0/resolver
- [ ] https://github.com/daehee/mildew
- [ ] https://github.com/haccer/subjack
- [ ] https://github.com/celrenheit/lion
- [ ] https://github.com/projectdiscovery/dnsprobe
- [ ] https://github.com/Cgboal/SonarSearch
- [ ] https://github.com/jakejarvis/subtake
- [ ] https://github.com/jimen0/fdns
- [ ] https://github.com/subfinder/research
- [ ] https://github.com/CosasDePuma/Elliot
- [ ] https://github.com/ManShum812/Automating-Scan-Live-Subdomain
- [ ] https://github.com/jonhadfield/subtocheck
- [ ] https://github.com/yunxu1/dnsub
- [ ] https://github.com/hsw409328/go-scan
- [ ] https://github.com/Freek3r/GoDigDomain

二、路径扫描

- [x] https://github.com/ffuf/ffuf
- [ ] https://github.com/1c3z/fileleak
- [ ] https://github.com/liamg/scout
- [ ] https://github.com/Matir/webborer
- [ ] https://github.com/hakluke/hakrawler
- [ ] https://github.com/evilsocket/dirsearch
- [ ] https://github.com/sdgdsffdsfff/lcyscan
- [ ] https://github.com/RedTeamPentesting/monsoon
- [ ] https://github.com/yuxiaokui/webfinger
- [ ] https://github.com/projectdiscovery/nuclei

三、端口扫描

- [ ] https://github.com/awake1t/PortBrute
- [ ] https://github.com/liamg/furious
- [ ] https://github.com/zmap/zgrab2
- [ ] https://github.com/vus520/go-scan
- [ ] https://github.com/ariagecheney/fyneIPinfo
- [ ] https://github.com/FDlucifer/goportscan
- [ ] https://github.com/youshu/GoScanner
- [ ] https://github.com/Ullaakut/nmap
- [ ] https://github.com/lavalamp-/ipv666
- [ ] https://github.com/timest/goscan
- [ ] https://github.com/projectdiscovery/naabu
- [ ] https://github.com/nray-scanner/nray
- [ ] https://github.com/projectdiscovery/naabu

四、密码爆破

- [ ] https://github.com/topics/bruteforce?l=go
- [x] https://github.com/netxfly/x-crack
- [ ] https://github.com/oksbsb/crack
- [ ] https://github.com/ncsa/ssh-auditor
- [ ] https://gitlab.com/opennota/hydra
- [ ] https://github.com/lazytools/sshchecker
- [ ] https://github.com/Shadow26Wolf/quickbrute
- [ ] https://github.com/codexlynx/brutemq
- [ ] https://github.com/bigb0sss/goPassGen
- [ ] https://github.com/fireeye/gocrack
- [ ] https://github.com/china-muwenbo/goScanPort
- [ ] https://github.com/zhangqin/mysqlfuzz
- [ ] https://github.com/wrfly/ssh-goburp
- [ ] https://github.com/sysdream/ligolo

五、漏洞扫描

- [x] https://github.com/bp0lr/wurl
- [x] https://github.com/jaeles-project/jaeles
- [x] https://github.com/r0lh/CVE-2019-8449
- [x] https://github.com/labrusca/Struts2-045-checker
- [x] https://github.com/brompwnie/CVE-2019-5418-Scanner
- [x] https://github.com/irealing/banner
- [x] https://github.com/lovetrap/SafeScan
- [x] https://github.com/45253321/gogoscan
- [x] https://github.com/projectdiscovery/httpx
- [ ] https://github.com/Any3ite/phpstudy_backdoor
- [ ] https://github.com/Any3ite/CVE-2020-5902-F5BIG
- [ ] https://github.com/Any3ite/CVE-2014-6271
- [ ] https://github.com/Any3ite/cdnCheck
- [ ] https://github.com/projectdiscovery/nuclei
- [ ] https://github.com/jaeles-project/gospider
- [ ] https://github.com/awake1t/linglong
- [ ] https://github.com/optiv/CVE-2020-15931
- [ ] https://github.com/r0lh/vubase
- [ ] https://github.com/Ne0nd0g/merlin
- [ ] https://github.com/leobeosab/sharingan
- [ ] https://github.com/chaitin/xray
- [ ] https://github.com/teknogeek/ssrf-sheriff
- [ ] https://github.com/awake1t/PortBrute
- [ ] https://github.com/gobysec/Goby
- [ ] https://github.com/opensec-cn/kunpeng
- [ ] https://github.com/x51/STS2G
- [ ] https://github.com/Adminisme/ServerScan
- [ ] https://github.com/jjf012/gopoc
- [ ] https://github.com/dwisiswant0/crlfuzz
- [ ] https://github.com/boy-hack/goWhatweb
- [ ] https://github.com/l3m0n/whatweb
- [ ] https://github.com/boy-hack/goWhatweb
- [ ] https://github.com/ethicalhackingplayground/Zin
- [ ] https://github.com/ring04h/s5.go
- [ ] https://github.com/graniet/GoFuzz
- [ ] https://github.com/netxfly/xsec-proxy-scanner
- [ ] https://github.com/future-architect/vuls
- [ ] https://github.com/jpillora/icmpscan
- [ ] https://github.com/peterhellberg/xip.name
- [ ] https://github.com/aquasecurity/trivy
- [ ] https://github.com/LakeVilladom/goSkylar
- [ ] https://github.com/jmpews/goscan
- [ ] https://github.com/HToTH/fuckcdn
- [ ] https://github.com/AkinoMaple/dedecms-admin-scan
- [ ] https://github.com/irealing/shorturl
- [ ] https://github.com/luc10/struts-rce-cve-2017-9805
- [ ] https://github.com/realjf/sils
- [ ] https://github.com/dreddsa5dies/urlScrub
- [ ] https://github.com/hahwul/WebHackersWeapons
- [ ] https://github.com/yahoo/gryffin
- [ ] https://github.com/hack2fun/Gscan
- [ ] https://github.com/hack2fun/EventLogBypass
- [ ] https://github.com/hakluke/hakrawler
- [ ] https://github.com/jm33-m0/mec-ng
- [ ] https://github.com/hahwul/hack-pet
- [ ] https://github.com/s-index/go-cve-search
- [ ] https://github.com/markwh245/rapt0r

六、隧道代理

- [ ] https://github.com/fatedier/frp
- [ ] https://github.com/Dliv3/Venom
- [ ] https://github.com/danielan/lonely-shell
- [ ] https://github.com/sysdream/ligolo
- [ ] https://github.com/sysdream/hershell
- [ ] https://github.com/sysdream/chashell
- [ ] https://github.com/cw1997/NATBypass
- [ ] https://github.com/ph4ntonn/go-socks5
- [ ] https://github.com/ph4ntonn/Stowaway
- [ ] https://github.com/LubyRuffy/tcptunnel
- [ ] https://github.com/FireFart/goshell
- [ ] https://github.com/ypcd/gstunnel
- [ ] https://github.com/ginuerzh/gost
- [ ] https://github.com/xct/xc
- [ ] https://github.com/jpillora/chisel
- [ ] https://github.com/snail007/goproxy
- [ ] https://github.com/esrrhs/pingtunnel
- [ ] https://github.com/whitehatnote/BlueShell
- [ ] https://github.com/averagesecurityguy/c2
- [ ] https://github.com/tiagorlampert/CHAOS
- [ ] https://github.com/e3prom/ruse
- [ ] https://github.com/google/martian
- [x] https://github.com/rabbitmask/Netstat
- [ ] https://github.com/wrfly/gus-proxy
- [ ] https://github.com/HTFTIMEONE/proxyPool
- [ ] https://github.com/TheKingOfDuck/ReverseGoShell
- [ ] https://github.com/WangYihang/Platypus

七、病毒免杀

- [ ] https://github.com/jax777/shellcode-launch
- [ ] https://github.com/EgeBalci/HERCULES
- [ ] https://github.com/petercunha/GoAT
- [ ] https://github.com/brimstone/go-shellcode
- [ ] https://github.com/ryhanson/phishery
- [ ] https://github.com/tanc7/EXOCET-AV-Evasion

八、代码审计

- [ ] https://github.com/tfsec/tfsec

九、其他项目

- [x] https://github.com/dstotijn/hetty
- [x] https://github.com/zsdevX/DarkEye
- [x] https://github.com/evilsocket/brutemachine
- [ ] https://github.com/evilsocket/xray
- [ ] https://github.com/bettercap/bettercap
- [ ] https://github.com/dreadl0ck/netcap
- [ ] https://github.com/projectdiscovery/nuclei
- [ ] https://github.com/haccer/subjack
- [ ] https://github.com/parsiya/Go-Security
- [ ] https://github.com/hahwul/dalfox
- [ ] https://github.com/ATpiu/asset-scan
- [ ] https://github.com/levidurfee/gowafp
- [ ] https://github.com/jeromer/mumbojumbo
- [ ] https://github.com/graniet/GoFuzz
- [ ] https://github.com/m-mizutani/lurker
- [ ] https://github.com/amanvir/enumerator
- [ ] https://github.com/izanbf1803/Go-Steganography
- [ ] https://github.com/ndelphit/apkurlgrep
- [ ] https://github.com/stevenaldinger/decker
- [ ] https://github.com/tucnak/telebot
- [ ] https://github.com/parsiya/Go-Security
- [ ] https://github.com/securego/gosec
- [ ] https://github.com/n0ncetonic/nmapxml
- [ ] https://github.com/omaidf/FaizEye
- [ ] https://github.com/jpillora/renamer
- [ ] https://github.com/jpillora/go-tcp-proxy
- [ ] https://github.com/projectdiscovery/httpx
- [ ] https://github.com/schollz/croc
- [ ] https://github.com/juju/utils
- [ ] https://github.com/evilsocket/xray
- [ ] https://github.com/dreadl0ck/netcap
- [ ] https://github.com/chenziyi920/file-scanMD5
- [ ] https://github.com/marco-lancini/goscan
- [ ] https://github.com/vulsio/go-exploitdb
- [ ] https://github.com/vulsio/msfdb-list-updater
- [ ] https://github.com/aquasecurity/vuln-list-update
- [ ] https://github.com/vyrus001/go-mimikatz
- [ ] https://github.com/jas502n/xray-crack
- [ ] https://github.com/codingo/bbr
- [ ] https://github.com/BishopFox/sliver
- [ ] https://github.com/zznop/sploit
- [ ] https://github.com/michenriksen/aquatone

## 0X07-Golang逆向

一、使用Go逆向

- [x] https://github.com/zznop/sploit
- [ ] https://www.anquanke.com/member/122079
- [ ] https://github.com/CarveSystems/gostringsr2
- [ ] https://blog.lgf.im/2020/golang-reverse-thinking.html

二、逆向Go程序

## 0x08-Golang老师

排名不分先后，记录自己在学习Go语言过程中遇到的这些优秀的Golang老师们。感谢感谢！

一、Go教程

- 柴树杉-Go语言高级编程作者@[chai2010](https://github.com/chai2010)
- 无闻-知名Go语言分享者@[Unknwon](https://github.com/Unknwon)、[博客](https://unknwon.cn/)
- 徐新华-studygolang站长@[polarisxu](https://github.com/polaris1119)
- 杨文-Go夜读发起人@[yangwenmai](https://github.com/yangwenmai)
- 陈剑煜-《Go语言编程之旅》作者@[eddycjy](https://github.com/eddycjy)、[博客](https://eddycjy.com/)
- 雨痕-《Go学习笔记》作者@[Q.yuhen](https://github.com/qyuhen)
- 谢孟军-beego作者[@astaxie](https://github.com/astaxie)

二、Go开发

- [https://github.com/netxfly](https://github.com/netxfly)
- https://github.com/schollz
- https://github.com/projectdiscovery
- https://github.com/aquasecurity
- https://github.com/projectdiscovery
